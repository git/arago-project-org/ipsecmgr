#!/bin/sh
# Modify & source this script before starting the build for IPSecMgr

ARAGO_DIR=~/sc-mcsdk/arago-tmp/sysroots/armv7a-none-linux-gnueabi

# Cross tool prefix
export CROSS_COMPILE=arm-none-linux-gnueabi-
export CC=${CROSS_COMPILE}gcc
export AR=${CROSS_COMPILE}ar

# Kernel include directory
export KERNEL_INC_DIR=$ARAGO_DIR/usr/include

# Libnl include paths
export LIBNL_INC_DIR=$ARAGO_DIR/usr/include

