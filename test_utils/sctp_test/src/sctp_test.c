/*
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/* Standard include files */
#include <stdint.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <netinet/sctp.h>

#define MAX_NUM_CHANNELS    8
#define MAX_PACKET_SIZE     64 * 1024

int min_pkt_size, max_pkt_size, num_channels, server_port, server_port;

/* List of socket fd's */
int fd_list[MAX_NUM_CHANNELS];
int fdclient_list[MAX_NUM_CHANNELS];

/* Channel stat */
struct chan_stat_s {
    int num_pkts_sent;
    int num_pkts_recv;
    int num_pkts_rxvaldnerror;
} chan_stats[MAX_NUM_CHANNELS];

char recvBuf [MAX_PACKET_SIZE+1];

int run_sctp_client (char* dstIPAddr)
{
    int                     i, pkt_size, retval, j, flags;
    struct sockaddr_in      server_sa, rem_sa;
    struct sctp_initmsg     initmsg = {0};
    char*                   sendBuf;
    struct sctp_sndrcvinfo  sndrcvinfo = {0};
    ssize_t                 rlen;
    socklen_t               from_len;

    // Initializations 
    memset(&chan_stats, 0, sizeof(chan_stats));

    for (i = 0; i < num_channels; i ++) {       
        //open client socket
        if ((fd_list[i] = socket(AF_INET, SOCK_STREAM, IPPROTO_SCTP)) < 0) {
            printf("Error: socket () failed with  %d\n", errno);
            return -1;
        }

        //setup the socket options
        initmsg.sinit_num_ostreams = 1;  //number of output streams can be greater
        if (setsockopt(fd_list[i], IPPROTO_SCTP, SCTP_INITMSG, &initmsg, sizeof(initmsg)) < 0) {
            printf("Error: setsockopt() failed with %d\n", errno);
            return -1;
        }

        //setup remote endpoint to connect to
        bzero ((void *)&server_sa, sizeof(server_sa));
        server_sa.sin_family = AF_INET;
        server_sa.sin_addr.s_addr = inet_addr (dstIPAddr);
        server_sa.sin_port = htons(server_port + i);

        //establish SCTP association
        if (connect(fd_list[i], (struct sockaddr *)&server_sa, sizeof(server_sa)) < 0) {
               printf("Error: connect () failed with %d\n", errno);
               return -1;
        }

        //check status
        from_len = (socklen_t) sizeof(struct sctp_status);
        struct sctp_status status = {0};
        if (getsockopt(fd_list[i], IPPROTO_SCTP, SCTP_STATUS, &status, &from_len) < 0) {
            printf("Error: getsockopt() returned error %d\n", errno);
            return -1;
        } 
        else {
            printf("Debug: Client socket connected, Association ID\t\t= %d, Receiver window size\t= %d\n", 
                    status.sstat_assoc_id, status.sstat_rwnd);
       }
    }

    while (1)
    {
        // Send data in increments of 64 bytes            
        for (pkt_size = min_pkt_size; pkt_size <= max_pkt_size; pkt_size += 64)
        {
            //allocate and initialize send buffer
            if ((sendBuf = malloc (pkt_size)) == NULL) {
                printf ("Error: malloc() failed for %d size \n", pkt_size);
                return -1;
            }
            for (j = 0; j <pkt_size; j++) sendBuf[j] = 0xAA;

            for (i = 0; i < num_channels; i++)
            {
                //send message to server
                if (sctp_sendmsg(fd_list[i], (const void *)sendBuf, pkt_size, NULL, 0, 0, 0, 0, 0, 0) < 0) {
                    printf ("Error: sctp_sendmsg () failed for channel %d with error %d \n", i, errno);
                    return -1;
                }
                chan_stats[i].num_pkts_sent++;

                //reset all Rx params
                flags = 0;
                memset((void *)&rem_sa, 0, sizeof (struct sockaddr_in));
                from_len    =   (socklen_t) sizeof(struct sockaddr_in);
                memset ((void *)&sndrcvinfo, 0, sizeof(struct sctp_sndrcvinfo));

                //receive and validate data
                if ((rlen = sctp_recvmsg(fd_list[i], recvBuf, MAX_PACKET_SIZE, (struct sockaddr *)&rem_sa, &from_len, &sndrcvinfo, &flags)) < 0) {
                    //printf("Error: sctp_recvmsg() failed\n");
                    continue;
                    //return -1;
                }
                chan_stats[i].num_pkts_recv++;

                if (rem_sa.sin_port != htons(server_port + i) || pkt_size != rlen || memcmp((const void *)sendBuf, (const void *)recvBuf, rlen)) {
                    //printf("Error: data mismatch for channel(%d)\n", i); 
                    chan_stats[i].num_pkts_rxvaldnerror ++;
                }
            }
            free(sendBuf);
        }
    }
}

int run_sctp_server (char* serverIPAddr)
{
    int                         i, retval, num_chans_processed, flags, rlen;
    struct sockaddr_in          server_sa, rem_sa;
    struct sctp_event_subscribe event = {0};
    struct sctp_sndrcvinfo      sndrcvinfo = {0};
    struct sctp_initmsg         initmsg = {0};
    socklen_t                   from_len;
    int                         optval;

    //Initializations
    memset(&chan_stats, 0, sizeof(chan_stats));

    for (i = 0; i < num_channels; i ++) {       

        //setup socket params            
        bzero ((void *)&server_sa, sizeof (server_sa));
        server_sa.sin_family = AF_INET;
        server_sa.sin_addr.s_addr = inet_addr(serverIPAddr);
        server_sa.sin_port = htons(server_port + i);

        //open client socket
        if ((fd_list[i] = socket(AF_INET, SOCK_SEQPACKET, IPPROTO_SCTP)) < 0) {
            printf("Error: socket () failed with  %d\n", errno);
            return;
        }

#if 0
        //make sure we receive MSG_NOTIFICATION
        if (setsockopt(fd_list[i], IPPROTO_SCTP, SCTP_EVENTS, &event, sizeof(struct sctp_event_subscribe)) < 0) {
            printf ("Error: setsockopt() failed with error %d \n", errno);
            return -1;
        }
#endif

        //bind to the port
        if (bind(fd_list[i],(struct sockaddr *)&server_sa, sizeof(server_sa)) < 0) {
            printf("Error: bind() failed with error %d\n", errno);
            return -1;
        }

        // wait for connections 
        if (listen (fd_list[i], 1) < 0) {
            printf ("Error: listen() failed with error %d\n", errno);
            return -1;
        }
    }

    while (1)
    {
        for (i=0; i<num_channels; i++) {
            //reset all Rx params                
            flags = 0;
            memset((void *)&rem_sa, 0, sizeof (struct sockaddr_in));
            from_len    =   (socklen_t) sizeof(struct sockaddr_in);
            memset ((void *)&sndrcvinfo, 0, sizeof(struct sctp_sndrcvinfo));

            //receive data from client
            rlen = sctp_recvmsg(fd_list[i], recvBuf, MAX_PACKET_SIZE, (struct sockaddr *)&rem_sa, &from_len, &sndrcvinfo, &flags);
            if (rlen < 0) {
                printf("Error: sctp_recvmsg() failed\n");
                continue;
            }
            chan_stats[i].num_pkts_recv++;

            //echo back data to client
            if (sctp_sendmsg(fd_list[i], (const void *)recvBuf, rlen, (struct sockaddr*)&rem_sa, from_len, 0, 0, 0, 0, 0) < 0) {
                printf ("Error: sctp_sendmsg () failed for channel %d with error %d \n", i, errno);
                return -1;
            }
            chan_stats[i].num_pkts_sent++;
        }
    }
}

void print_stats ()
{
    int i;

    printf("channel stats:\n");
    for (i=0; i<num_channels; i++) {
        printf("ch(%d): pkts_sent=%d : pkts_recvd=%d : pkts_rxvaldnerrors=%d\n", i,
               chan_stats[i].num_pkts_sent, chan_stats[i].num_pkts_recv,  chan_stats[i].num_pkts_rxvaldnerror);
    }
}

void app_shutdown(void)
{
    int i;
    
    for (i=0; i<num_channels; i++) {
        if (fd_list[i]) close(fd_list[i]);
    }

    return;
}

void term_handler (int signal)
{
    print_stats ();
    app_shutdown ();
    exit (1);
}

#define SCTP_CLIENT_ARGS "dst_ip dst_port num_channels min_pkt_size max_pkt_size" 
#define SCTP_SERVER_ARGS "src_ip src_port num_channels" 
int main (int argc, char** argv)
{
    sa_family_t         af = AF_INET; //No support for IPv6 as of now
    struct sockaddr_in  sa;
    int                 ret;

    if (argc<2 || (argv[1][1] != 'c' && argv[1][1] != 's')) {
        printf ("Options: \n");
        printf ("-s: Run as SCTP server\n");
        printf ("-c: Run as SCTP client\n");
        return -1;
    }

    signal (SIGINT, &term_handler);
    signal (SIGTERM, &term_handler);
    signal (SIGHUP, &term_handler);
    signal (SIGKILL, &term_handler);
    signal (SIGABRT, &term_handler);

    memset (fd_list, 0, sizeof(int) * MAX_NUM_CHANNELS);

    if (argv[1][1] == 'c') {
        /* User input validations for SCTP client */            
        if (argc != 7) {
            printf("USAGE: %s -c %s\n", argv[0], SCTP_CLIENT_ARGS);
            return -1;
        }

        ret = inet_pton(af, argv[2], &sa.sin_addr);
        if (ret <= 0) {
            printf("ERROR: dst_ip not in presentation format\n");
            return -1;
        }
        server_port = atoi(argv[3]);

        num_channels = atoi(argv[4]);
        if (!(num_channels) || (num_channels > MAX_NUM_CHANNELS)) {
            printf ("ERROR: max SCTP channels supported is %d\n", MAX_NUM_CHANNELS);
            return -1;
        }

        min_pkt_size = atoi(argv[5]);
        max_pkt_size = atoi(argv[6]);

        run_sctp_client (argv[2]);
    }
    else if (argv[1][1] == 's') {
        /* User input validations for SCTP server */            
        if (argc != 5) {
            printf("USAGE: %s -s %s\n", argv[0], SCTP_SERVER_ARGS);
            return -1;
        }

        ret = inet_pton(af, argv[2], &sa.sin_addr);
        if (ret <= 0) {
            printf("ERROR: src_ip not in presentation format\n");
            return -1;
        }
        server_port = atoi(argv[3]);

        num_channels = atoi(argv[4]);
        if (!(num_channels) || (num_channels > MAX_NUM_CHANNELS)) {
            printf ("ERROR: max SCTP channels supported is %d\n", MAX_NUM_CHANNELS);
            return -1;
        }
        run_sctp_server (argv[2]);
    }
    else {
        printf("Invalid option \n");
        return -1;
    }

shut_down:
    print_stats();
    /* Shutdown */
    app_shutdown();
    return 0;
}
