/*
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/* Standard include files */
#include <stdint.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>

#define MAX_UDP_CHANNELS    8

/* Number of channels in use */
int num_channels;

/* List of socket fd's */
int fd_list[MAX_UDP_CHANNELS];

/* buffer to store pkts to send/recv */
uint8_t *pkt_buf_recv = NULL;

/* Size of packet to send */
int pkt_sz;

/* Data used in select() */
struct sel_ctx_s {
    int             nfds;
    fd_set          readfds;
    struct timeval  timeout;
} sel_ctx;

/* Channel stat */
struct chan_stat_s {
    int num_pkts_sent;
    int num_pkts_recv;
} chan_stats[MAX_UDP_CHANNELS];

int init_pkt_buffers(int pkt_sz) {
    int i;
    if ((pkt_buf_recv = malloc(pkt_sz)) == NULL) {
        return -1;
    }
    return 0;
}

int32_t get_my_ipaddr(char *local_if_name, sa_family_t af, struct sockaddr *sock)
{
    struct ifaddrs *myaddrs, *ifa;
    int status;
    struct sockaddr_in *psock;

    status = getifaddrs(&myaddrs);
    if (status != 0) {
        perror("getifaddrs failed!");
        return -1;
    }

    for (ifa = myaddrs; ifa != NULL; ifa = ifa->ifa_next){
        if (NULL == ifa->ifa_addr){
            continue;
        }

        if ((ifa->ifa_flags & IFF_UP) == 0) {
            continue;
        }

        if ((ifa->ifa_addr->sa_family != af) || (strcmp(ifa->ifa_name, local_if_name))) {
            continue;
        }
        
        *sock = *ifa->ifa_addr; 
        status = 1;
        break;
    }

    freeifaddrs(myaddrs);

    if (status != 1) return -1;

    return 0;
}

int32_t init_socket(struct sockaddr *sock, int *sock_fd)
{
    if (sock->sa_family == AF_INET) {
        *sock_fd = socket(AF_INET, SOCK_DGRAM, 0);

    } else if (sock->sa_family == AF_INET6) {
        /* TODO: IPv6 INIT */
        printf("ERROR: IPv6 not supported\n");
        return -1;
    }

    if (*sock_fd < 0) {
        perror("ERROR: error opening socket");
        return -1;
    }
    /* IPv4 socket bind */
    if (sock->sa_family == AF_INET) {
        if (bind(*sock_fd, sock, sizeof(struct sockaddr_in)) < 0) {
            perror("ERROR: error binding socket");
            close(*sock_fd);
            return -1;
        }
    }
    else {
        /* TODO: IPv6 bind */
        return -1;
    }

    if (*sock_fd > (sel_ctx.nfds-1)) {
        sel_ctx.nfds = *sock_fd + 1;
    }

    FD_SET(*sock_fd, &(sel_ctx.readfds));
    sel_ctx.timeout.tv_sec = 10;
    sel_ctx.timeout.tv_usec = 0;

    return 0;
}

int32_t pkt_send(uint8_t *buf, uint16_t len, int ch_num, struct sockaddr *dst)
{
    if (sendto(fd_list[ch_num], buf, len, 0, (const struct sockaddr *)dst,
               sizeof(struct sockaddr_in))==-1) 
    {
        perror("ERROR: error sending packet");
        return -1;
    }
    chan_stats[ch_num].num_pkts_sent++;
    return 0;
}

int32_t pkt_lpbk()
{
    struct sockaddr rem_sock;
    int retval, ch_cnt=0;
    fd_set readfds = sel_ctx.readfds;
    ssize_t rlen;
    socklen_t slen;
    int i;
 
do_select:
    retval = select(sel_ctx.nfds, &readfds, NULL, NULL, &sel_ctx.timeout);
    if (retval == -1)
    {
        perror("ERROR: select failed\n");
        return -1;
    } 

    /* select timeout */
    if (retval == 0) {
        return 0;
    }

    ch_cnt += retval;

    for (i=0; i<num_channels; i++) {
        if (FD_ISSET(fd_list[i], &readfds)) {
            slen = sizeof(struct sockaddr);
            if ((rlen = recvfrom(fd_list[i], pkt_buf_recv, pkt_sz, 0,
                                 &rem_sock, &slen)) == -1) {
                perror("ERROR: recvfrom failed\n");
                return -1;
            }

            chan_stats[i].num_pkts_recv++;

            /* Send packet */
            if (pkt_send(pkt_buf_recv, rlen, i, &rem_sock)) {
                printf("ERROR: Failed to send pkt for ch(%d)\n", i);
                return -1;
            }
        }
    }

    if (ch_cnt != num_channels) {
        readfds = sel_ctx.readfds;
        goto do_select;
    }

    return 0;
}

void app_shutdown(void)
{
    int i;
    
    for (i=0; i<num_channels; i++) {
        if (fd_list[i]) close(fd_list[i]);
    }

    if (pkt_buf_recv) free(pkt_buf_recv);
    return;
}

void print_stats()
{
    int i;
    printf("channel stats:\n");
    for (i=0; i<num_channels; i++) {
        printf("ch(%d): pkts_recvd=%d : pkts_sent=%d\n", i,
               chan_stats[i].num_pkts_recv, chan_stats[i].num_pkts_sent);
    }
    return;
}

/* termination signal handler */
static void termination_handler(int signum)
{
    print_stats();
    app_shutdown();
    exit(0);
}


#define ARGS "IPv4 if_name  num_channels start_port max_pkt_sz" 
int main (int argc, char *argv[])
{
    sa_family_t af;
    struct sockaddr_in dsa;
    struct sockaddr_in my_addr;
    int i, j, ret, dst_st_port, src_st_port, num_pkts;

    if (argc != 6) {
        printf("USAGE: %s %s\n", argv[0], ARGS);
        return -1;
    }

    if (!strcmp(argv[1],"IPv4")) {
        af = AF_INET;
    } else {
        printf ("ERROR:  Invalid address family %s\n", argv[1]);
        return -1;
    }

    num_channels = atoi(argv[3]);

    if (!(num_channels) || (num_channels > MAX_UDP_CHANNELS)) {
        printf ("ERROR: max channels supported is %d\n", MAX_UDP_CHANNELS);
        return -1;
    }

    src_st_port = atoi(argv[4]);
    pkt_sz = atoi(argv[5]);

    /* initializations */
    memset(fd_list, 0, sizeof(fd_list));
    memset(&sel_ctx, 0, sizeof(sel_ctx));
    sel_ctx.nfds = 1;
    memset(&chan_stats, 0, sizeof(chan_stats));

    if (init_pkt_buffers(pkt_sz)) {
        printf("ERROR: Failed to allocate buffer for pkts\n");
        return -1;
    }

    /* Get self IP addr */
    if (get_my_ipaddr(argv[2], af, (struct sockaddr *)&my_addr)) {
        printf("ERROR: Failed to retrieve own IP address\n");
        return -1;
    }

    /* Initialize socket for each channel */
    for (i=0; i<num_channels; i++) {
        struct sockaddr_in *sptr;
        my_addr.sin_port = htons(src_st_port + i);
        if (init_socket((struct sockaddr *)&my_addr, &fd_list[i])) {
            printf("ERROR: Failed to init socket\n");
            app_shutdown();
            return -1;
        }
    }

    /* Cancel certain signals */
    signal(SIGCHLD,SIG_DFL); /* A child process dies */
    signal(SIGTSTP,SIG_IGN); /* Various TTY signals */
    signal(SIGTTOU,SIG_IGN);
    signal(SIGTTIN,SIG_IGN);
    signal(SIGHUP, SIG_IGN); /* Ignore hangup signal */
    signal(SIGINT,termination_handler); /* catch ctrl-c */
    signal(SIGTERM,termination_handler); /* catch kill */

    /* recv/send loop */
    while (1) {
        if (pkt_lpbk()) {
            printf("ERROR: pkt_lpbk failed\n");
            goto shut_down;
        }
    }

shut_down:
    print_stats();
    /* Shutdown */
    app_shutdown();
    return 0;
}


