/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef __IPSECMGR__IPC_H__
#define __IPSECMGR__IPC_H__

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <ipsecmgr_types.h>

/* IPC user mode */
typedef enum {
    IPC_MODE_USER_APP     = 1,
    IPC_MODE_SNOOP_DAEMON = 2
} ipsecmgr_ipc_mode_t;

/* IPC address */
typedef union {
    struct sockaddr_un  sock_addr;
} ipsecmgr_ipc_addr_t;

/* IPC library configuration */
typedef struct {
    ipsecmgr_ipc_mode_t mode;
    ipsecmgr_ipc_addr_t *my_addr;
    ipsecmgr_ipc_addr_t *remote_addr;
    uint32_t ipc_id;
} ipsecmgr_ipc_cfg_t;

/* Offload SP request parameters */
typedef struct {
    ipsecmgr_trans_id_t     trans_id; /* Transaction id of the request */
    ipsecmgr_policy_id_t    policy_id;/* Security Policy ID in the kernel */
    ipsecmgr_ifname_t       *if_name; /* MAC interface name; required only for
                                         ingress policy */
    ipsecmgr_sa_dscp_map_cfg_t *dscp_cfg;

    /* Layer 5 selector.
     * This is an optional parameter */
    ipsecmgr_l5_selector_t  *l5_selector;

    /* Additional SA flags */
    ipsecmgr_sa_flags_t     sa_flags;

} ipsecmgr_ipc_offload_sp_req_param_t;

/* Stop offload SP request parameters */
typedef struct {
    ipsecmgr_trans_id_t     trans_id; /* Transaction id of the request */
    ipsecmgr_policy_id_t    policy_id;/* Security Policy ID in the kernel */
    ipsecmgr_no_expire_sa_t no_expire_sa; /* Flag to indicate not to expire Sa on
                                             stop_offload_request */
} ipsecmgr_ipc_stop_offload_req_param_t;

/* Stop offload SP request parameters */
typedef struct {
    ipsecmgr_trans_id_t     trans_id; /* Transaction id of the request */
    ipsecmgr_rsp_type_t     type;
    ipsecmgr_result_t       result;
} ipsecmgr_ipc_stop_offload_rsp_param_t;

/* Offload SP response parameters */
typedef struct {
    ipsecmgr_rsp_type_t     type;
    ipsecmgr_result_t       result;
    uint32_t                err_code; /* NETCP lib err code */
    ipsecmgr_trans_id_t     trans_id; /* Transaction id to correlate request */
    ipsecmgr_fp_handle_t    sp_handle; 
    ipsecmgr_fp_handle_t    sa_handle; 
} ipsecmgr_ipc_offload_sp_rsp_param_t;

/* IPC recieve i/f for user */
typedef struct {
    void (*offload_sp_rsp)
    (
        ipsecmgr_ipc_offload_sp_rsp_param_t *rsp
    );
    void (*stop_offload_rsp)
    (
        ipsecmgr_ipc_stop_offload_rsp_param_t *rsp
    );
} ipsecmgr_ipc_user_recv_if_t;

/* IPC send i/f for user */
typedef struct {
    int (*offload_sp_req)
    (
        ipsecmgr_ipc_offload_sp_req_param_t *req
    );
    int (*stop_offload_req)
    (
        ipsecmgr_ipc_stop_offload_req_param_t *req
    );
} ipsecmgr_ipc_user_send_if_t;

/* IPC recieve i/f for ipsecmgr daemon */
typedef struct {
    void (*offload_sp_req)
    (
        ipsecmgr_ipc_offload_sp_req_param_t *req,
        ipsecmgr_ipc_addr_t *src_addr
    );
    void (*stop_offload_req)
    (
        ipsecmgr_ipc_stop_offload_req_param_t *req,
        ipsecmgr_ipc_addr_t *src_addr
    );
} ipsecmgr_ipc_daemon_recv_if_t;

/* IPC send i/f for ipsecmgr daemon */
typedef struct {
    int (*offload_sp_rsp)
    (
        ipsecmgr_ipc_offload_sp_rsp_param_t *rsp,
        ipsecmgr_ipc_addr_t *dst_addr
    );
    int (*stop_offload_rsp)
    (
        ipsecmgr_ipc_stop_offload_rsp_param_t *rsp,
        ipsecmgr_ipc_addr_t *dst_addr
    );
} ipsecmgr_ipc_daemon_send_if_t;

/* API's */

/* Initialize IPC library
 * If "cfg" is passed as NULL then the library retrieve the address
 * configuration from the following environment variables
 */ 
int ipsecmgr_ipc_init ( ipsecmgr_ipc_cfg_t *cfg );

/* Register user recieve i/f */
int ipsecmgr_ipc_register_user_recv_iface
(
    ipsecmgr_ipc_user_recv_if_t *recv_if
);

/* Get user send i/f */
int ipsecmgr_ipc_get_user_send_iface
(
    ipsecmgr_ipc_user_send_if_t **send_if
);

/* Register ipsecmgr daemon recieve i/f */
int ipsecmgr_ipc_register_daemon_recv_iface
(
    ipsecmgr_ipc_daemon_recv_if_t *recv_if
);

/* Get ipsecmgr daemon send i/f */
int ipsecmgr_ipc_get_daemon_send_iface
(
    ipsecmgr_ipc_daemon_send_if_t **send_if
);

/* Poll for IPC messages */
void ipsecmgr_ipc_poll ( void );

/* Shutdown IPC library */
void ipsecmgr_ipc_shutdown ( void );

#endif /* __IPSECMGR__IPC_H__ */

