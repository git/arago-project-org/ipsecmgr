/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <ipsecmgr_ipc.h>
#include <ipsecmgr_syslog.h>
#include "ipsecmgr_ipc_loc.h"
#include "ipsecmgr_ipc_msg.h"

#define IPC_INITIALIZED 0xABCDEF

#define PATH_MAX 108

/* Environment variable name for the application's UNIX domain socket name */
#define APP_SOCK_NAME   "IPSECMGR_APP_SOCK_NAME"

/* Environment variable name for the IPSec daemon's UNIX domain socket name */
#define DAEMON_SOCK_NAME   "IPSECMGR_DAEMON_SOCK_NAME"

static struct ipc_ctx_s {
    ipsecmgr_ipc_user_recv_if_t     *user_recv_if;
    ipsecmgr_ipc_user_send_if_t     user_send_if;
    ipsecmgr_ipc_daemon_recv_if_t   *daemon_recv_if;
    ipsecmgr_ipc_daemon_send_if_t   daemon_send_if;
    ipc_unix_sock_ctx_t             unix_sock;
    uint32_t                        initialized;
    ipsecmgr_ipc_mode_t             mode;
    ipsecmgr_ipc_addr_t             snoop_daemon_addr;
} ipc_ctx;

static int ipsecmgr_ipc_send_offload_sp_req
(
    ipsecmgr_ipc_offload_sp_req_param_t *req
);

static int ipsecmgr_ipc_send_stop_offload_req
(
    ipsecmgr_ipc_stop_offload_req_param_t *req
);

static int ipsecmgr_ipc_send_offload_sp_rsp
(
    ipsecmgr_ipc_offload_sp_rsp_param_t *rsp,
    ipsecmgr_ipc_addr_t *dst_addr
);

static int ipsecmgr_ipc_send_stop_offload_rsp
(
    ipsecmgr_ipc_stop_offload_rsp_param_t *rsp,
    ipsecmgr_ipc_addr_t *dst_addr
);

static int ipsecmgr_ipc_get_app_addr(ipsecmgr_ipc_addr_t *addr, uint32_t ipc_id)
{
    char *sock_name;
    struct sockaddr_un *sock_addr = &(addr->sock_addr);
    char temp_str[PATH_MAX];

    sock_name = getenv(APP_SOCK_NAME);

    if (sock_name == NULL) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "IPC: Environment variable %s not set\n", APP_SOCK_NAME);
        return -1;
    }

    snprintf(&temp_str[0], PATH_MAX-1, "%s%d", sock_name,ipc_id);
    sock_addr->sun_family = AF_UNIX;
    strncpy(sock_addr->sun_path, &temp_str[0], sizeof(sock_addr->sun_path)-1);

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "Environment variable %s = %s\n", APP_SOCK_NAME, sock_name);
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "Environment variable temp %s = %s\n", APP_SOCK_NAME, &temp_str[0]);
    return 0;
}

static int ipsecmgr_ipc_get_daemon_addr(ipsecmgr_ipc_addr_t *addr, uint32_t ipc_id)
{
    char *sock_name;
    struct sockaddr_un *sock_addr = &(addr->sock_addr);
    char temp_str[PATH_MAX];

    sock_name = getenv(DAEMON_SOCK_NAME);

    if (sock_name == NULL) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "Environment variable %s not set\n", DAEMON_SOCK_NAME);
        return -1;
    }
    snprintf(&temp_str[0], PATH_MAX-1, "%s%d", sock_name,ipc_id);

    strncpy(sock_addr->sun_path, &temp_str[0], sizeof(sock_addr->sun_path)-1);
    sock_addr->sun_family = AF_UNIX;

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "Environment variable temp %s = %s\n", DAEMON_SOCK_NAME, &temp_str[0]);
    return 0;
}

int ipsecmgr_ipc_init ( ipsecmgr_ipc_cfg_t *cfg )
{
    ipsecmgr_ipc_addr_t my_addr;
    ipsecmgr_ipc_addr_t remote_addr;

    if (!cfg) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "ipc: ERROR: No configuration\n");
        return -1;
    }

    if ((cfg->mode != IPC_MODE_USER_APP) &&
        (cfg->mode != IPC_MODE_SNOOP_DAEMON)) {

        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR, 
            "ipc: ERROR: Invalid mode param\n");
        return -1;
    }

    memset (&ipc_ctx, 0, sizeof(ipc_ctx));

    if (!cfg->my_addr) {
        cfg->my_addr = &my_addr;
        if (cfg->mode == IPC_MODE_SNOOP_DAEMON) {
            if (ipsecmgr_ipc_get_daemon_addr(cfg->my_addr, cfg->ipc_id)) {
                ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR, 
                    "ipc: ERROR: Could get sock addr for Daemon\n");
                return -1;
            }
        } else {
            if (ipsecmgr_ipc_get_app_addr(cfg->my_addr, cfg->ipc_id)) {
                ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR, 
                    "ipc: ERROR: Could get sock addr for App\n");
                return -1;
            }
        }
    }

    if (!cfg->remote_addr) {
        if (cfg->mode == IPC_MODE_USER_APP) {
            cfg->remote_addr = &remote_addr;
            if (ipsecmgr_ipc_get_daemon_addr(cfg->remote_addr,cfg->ipc_id)) {
                ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR, 
                    "ipc: ERROR: Could get sock addr for Daemon(remote)\n");
                return -1;
            }
            ipc_ctx.snoop_daemon_addr = *cfg->remote_addr;
        }
    }

    if (cfg->my_addr->sock_addr.sun_family != AF_UNIX) {

        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR, 
            "ipc: ERROR: Invalid socket address family for self\n");
        return -1;
    }

    if ((cfg->mode == IPC_MODE_USER_APP) &&
        (cfg->remote_addr->sock_addr.sun_family != AF_UNIX)) {

        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR, 
            "ipc: ERROR: Invalid socket address family for remote\n");
        return -1;
    }

    if ( ipsecmgr_ipc_unix_sock_init(cfg, &(ipc_ctx.unix_sock))) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR, 
            "ipc: ERROR: failed to initialize unix sock ipc\n");
        return -1;
    }

    ipc_ctx.mode = cfg->mode;
    ipc_ctx.user_send_if.offload_sp_req = ipsecmgr_ipc_send_offload_sp_req;
    ipc_ctx.user_send_if.stop_offload_req = ipsecmgr_ipc_send_stop_offload_req;
    ipc_ctx.daemon_send_if.offload_sp_rsp = ipsecmgr_ipc_send_offload_sp_rsp;
    ipc_ctx.daemon_send_if.stop_offload_rsp = ipsecmgr_ipc_send_stop_offload_rsp;
    ipc_ctx.initialized = IPC_INITIALIZED;
    return 0;
}

/* Register user recieve i/f */
int ipsecmgr_ipc_register_user_recv_iface
(
    ipsecmgr_ipc_user_recv_if_t *recv_if
)
{
    if (ipc_ctx.initialized != IPC_INITIALIZED)
        return -1;

    ipc_ctx.user_recv_if = recv_if;
    return 0;
}

/* Get user send i/f */
int ipsecmgr_ipc_get_user_send_iface
(
    ipsecmgr_ipc_user_send_if_t **send_if
)
{
    if (ipc_ctx.initialized != IPC_INITIALIZED)
        return -1;

    *send_if = &(ipc_ctx.user_send_if);
    return 0;
}

/* Register daemon recieve i/f */
int ipsecmgr_ipc_register_daemon_recv_iface
(
    ipsecmgr_ipc_daemon_recv_if_t *recv_if
)
{
    if (ipc_ctx.initialized != IPC_INITIALIZED)
        return -1;

    ipc_ctx.daemon_recv_if = recv_if;
    return 0;
}

/* Get daemon send i/f */
int ipsecmgr_ipc_get_daemon_send_iface
(
    ipsecmgr_ipc_daemon_send_if_t **send_if
)
{
    if (ipc_ctx.initialized != IPC_INITIALIZED)
        return -1;

    *send_if = &(ipc_ctx.daemon_send_if);
    return 0;
}

static int ipsecmgr_ipc_handle_offload_sp_rsp
(
    msg_t *msg
)
{
    ipsecmgr_ipc_offload_sp_rsp_param_t rsp;

    rsp.trans_id = msg->hdr.trans_id;
    rsp.type = msg->body.offload_sp_rsp.type;
    rsp.result = msg->body.offload_sp_rsp.result;
    rsp.err_code = msg->body.offload_sp_rsp.err_code;
    rsp.sp_handle = msg->body.offload_sp_rsp.sp_handle;
    rsp.sa_handle = msg->body.offload_sp_rsp.sa_handle;

    ipc_ctx.user_recv_if->offload_sp_rsp(&rsp);
    return 0;
}

static int ipsecmgr_ipc_handle_stop_offload_rsp
(
    msg_t *msg
)
{
    ipsecmgr_ipc_stop_offload_rsp_param_t rsp;

    rsp.trans_id = msg->hdr.trans_id;
    rsp.type = msg->body.stop_offload_rsp.type;
    rsp.result = msg->body.stop_offload_rsp.result;

    ipc_ctx.user_recv_if->stop_offload_rsp(&rsp);
    return 0;
}

static int ipsecmgr_ipc_handle_offload_sp_req
(
    msg_t *msg,
    ipsecmgr_ipc_addr_t *src
)
{
    ipsecmgr_ipc_offload_sp_req_param_t req;
    ipsecmgr_ifname_t  ifname;
    ipsecmgr_sa_dscp_map_cfg_t  dscp_cfg;
    ipsecmgr_l5_selector_t l5_sel;

    memset (&req, 0, sizeof(req));
    req.trans_id = msg->hdr.trans_id;
    req.policy_id = msg->body.offload_sp.policy_id;
    req.sa_flags = msg->body.offload_sp.sa_flags;
    
    if (msg->body.offload_sp.valid_param &
        IPSECMGR_IPC_MSG_OFFLOAD_SP_VALID_IFNAME) {
        ifname = msg->body.offload_sp.if_name;
        req.if_name = &ifname;
    }

    if (msg->body.offload_sp.valid_param &
        IPSECMGR_IPC_MSG_OFFLOAD_SP_VALID_DSCP_CFG) {
        dscp_cfg = msg->body.offload_sp.dscp_cfg;
        req.dscp_cfg = &dscp_cfg;
    }

    if (msg->body.offload_sp.valid_param &
        IPSECMGR_IPC_MSG_OFFLOAD_SP_VALID_L5_SEL) {
        l5_sel = msg->body.offload_sp.l5_selector;
        req.l5_selector = &l5_sel;
    }

    ipc_ctx.daemon_recv_if->offload_sp_req(&req, src);
    return 0;
}

static int ipsecmgr_ipc_handle_stop_offload_req
(
    msg_t *msg,
    ipsecmgr_ipc_addr_t *src
)
{
    ipsecmgr_ipc_stop_offload_req_param_t req;

    memset (&req, 0, sizeof(req));
    req.trans_id = msg->hdr.trans_id;
    req.policy_id = msg->body.stop_offload.policy_id;
    req.no_expire_sa = msg->body.stop_offload.no_expire_sa;
    ipc_ctx.daemon_recv_if->stop_offload_req(&req, src);
    return 0;
}

static void ipsecmgr_ipc_form_msg_hdr
(
    msg_hdr_t *msg_hdr,
    uint16_t msg_id,
    uint16_t msg_len,
    ipsecmgr_trans_id_t trans_id
) 
{
    msg_hdr->msg_id = msg_id;
    msg_hdr->msg_len = msg_len;
    msg_hdr->trans_id = trans_id;
}
    
static int ipsecmgr_ipc_send_offload_sp_req
(
    ipsecmgr_ipc_offload_sp_req_param_t *req
)
{
    msg_t msg;
    uint16_t len;

    memset(&msg, 0, sizeof(msg));
    ipsecmgr_ipc_form_msg_hdr (&msg.hdr, MSG_ID_OFFLOAD_SP,
                        sizeof(msg.body.offload_sp), req->trans_id);

    msg.body.offload_sp.policy_id = req->policy_id;
    msg.body.offload_sp.sa_flags = req->sa_flags;

    if (req->if_name) {
        memcpy(&(msg.body.offload_sp.if_name), req->if_name,
                sizeof(ipsecmgr_ifname_t));
        msg.body.offload_sp.valid_param |=
            IPSECMGR_IPC_MSG_OFFLOAD_SP_VALID_IFNAME;
    } 

    if (req->dscp_cfg) {
        memcpy(&(msg.body.offload_sp.dscp_cfg), req->dscp_cfg,
                sizeof(ipsecmgr_sa_dscp_map_cfg_t));
        msg.body.offload_sp.valid_param |=
            IPSECMGR_IPC_MSG_OFFLOAD_SP_VALID_DSCP_CFG;
    } 

    if (req->l5_selector) {
        memcpy(&(msg.body.offload_sp.l5_selector), req->l5_selector,
               sizeof(ipsecmgr_l5_selector_t));
        msg.body.offload_sp.valid_param |=
            IPSECMGR_IPC_MSG_OFFLOAD_SP_VALID_L5_SEL;
    } 

    len = sizeof(msg.hdr) + sizeof(msg.body.offload_sp);

    return ipsecmgr_ipc_unix_sock_send (&(ipc_ctx.unix_sock),
                    &msg, len, &ipc_ctx.snoop_daemon_addr);
}

static int ipsecmgr_ipc_send_stop_offload_req
(
    ipsecmgr_ipc_stop_offload_req_param_t *req
)
{
    msg_t msg;
    uint16_t len;

    memset(&msg, 0, sizeof(msg));
    ipsecmgr_ipc_form_msg_hdr (&msg.hdr, MSG_ID_STOP_OFFLOAD,
                        sizeof(msg.body.stop_offload), req->trans_id);

    msg.body.stop_offload.policy_id = req->policy_id;
    msg.body.stop_offload.no_expire_sa = req->no_expire_sa;

    len = sizeof(msg.hdr) + sizeof(msg.body.stop_offload);

    return ipsecmgr_ipc_unix_sock_send (&(ipc_ctx.unix_sock),
                    &msg, len, &ipc_ctx.snoop_daemon_addr);
}

static int ipsecmgr_ipc_send_stop_offload_rsp
(
    ipsecmgr_ipc_stop_offload_rsp_param_t *rsp,
    ipsecmgr_ipc_addr_t *dst_addr
)
{
    msg_t msg;
    uint16_t len;

    if (!rsp || !dst_addr)
        return -1;

    memset(&msg, 0, sizeof(msg));

    ipsecmgr_ipc_form_msg_hdr (&msg.hdr, MSG_ID_STOP_OFFLOAD_RSP,
                        sizeof(msg.body.stop_offload_rsp), rsp->trans_id);

    msg.body.stop_offload_rsp.type = rsp->type;
    msg.body.stop_offload_rsp.result = rsp->result;

    len = sizeof(msg.hdr) + sizeof(msg.body.stop_offload_rsp);

    return ipsecmgr_ipc_unix_sock_send (&(ipc_ctx.unix_sock),
                    &msg, len, dst_addr);
}

static int ipsecmgr_ipc_send_offload_sp_rsp
(
    ipsecmgr_ipc_offload_sp_rsp_param_t *rsp,
    ipsecmgr_ipc_addr_t *dst_addr
)
{
    msg_t msg;
    uint16_t len;

    if (!rsp || !dst_addr)
        return -1;

    memset(&msg, 0, sizeof(msg));
    ipsecmgr_ipc_form_msg_hdr (&msg.hdr, MSG_ID_OFFLOAD_SP_RSP,
                        sizeof(msg.body.offload_sp_rsp), rsp->trans_id);
    msg.body.offload_sp_rsp.sa_handle = rsp->sa_handle;
    msg.body.offload_sp_rsp.sp_handle = rsp->sp_handle;
    msg.body.offload_sp_rsp.type = rsp->type;
    msg.body.offload_sp_rsp.result = rsp->result;
    msg.body.offload_sp_rsp.err_code = rsp->err_code;

    len = sizeof(msg.hdr) + sizeof(msg.body.offload_sp_rsp);

    return ipsecmgr_ipc_unix_sock_send (&(ipc_ctx.unix_sock),
                    &msg, len, dst_addr);
}

static int ipsecmgr_ipc_handle_recvd_msgs
(
    msg_t *msg,
    ipsecmgr_ipc_addr_t *src
)
{
    if (ipc_ctx.mode == IPC_MODE_USER_APP) {
        switch(msg->hdr.msg_id) {
            case MSG_ID_OFFLOAD_SP_RSP:
                return ipsecmgr_ipc_handle_offload_sp_rsp(msg);
            break;

            case MSG_ID_STOP_OFFLOAD_RSP:
                return ipsecmgr_ipc_handle_stop_offload_rsp(msg);
            break;

            default:
                return -1;
        }
    } else {
        switch(msg->hdr.msg_id) {
            case MSG_ID_OFFLOAD_SP:
                return ipsecmgr_ipc_handle_offload_sp_req(msg, src);
            break;

            case MSG_ID_STOP_OFFLOAD:
                return ipsecmgr_ipc_handle_stop_offload_req(msg, src);
            break;

            default:
                return -1;
        }
    }	
    return 0;
}

/* Poll for IPC messages */
#define IPSECMGR_IPC_NUM_POLLS  2
void ipsecmgr_ipc_poll ( void )
{
    int max_polls = IPSECMGR_IPC_NUM_POLLS;
    int ret;
    msg_t msg;
    uint16_t len;
    ipsecmgr_ipc_addr_t src;

    if (ipc_ctx.initialized != IPC_INITIALIZED)
        return;

    while (max_polls--) {
        len = sizeof(msg);
        ret = ipsecmgr_ipc_unix_sock_recv
                        (&ipc_ctx.unix_sock, (uint8_t *)&msg, &len, &src);
        if (ret) {
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
                "IPC: ipc_poll: error in recv (%d)\n", ret);
            return;
        }

        if (len) {
            ret = ipsecmgr_ipc_handle_recvd_msgs(&msg, &src);
            if (ret) {
                ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
                    "IPC: ipc_poll: error in recvd ipsd msg id=%d err=%d\n",
                    msg.hdr.msg_id, ret);
            }
        }
    }
    return;
}

/* Shutdown IPC library */
void ipsecmgr_ipc_shutdown()
{
    ipsecmgr_ipc_unix_socket_close(&(ipc_ctx.unix_sock));
    memset (&ipc_ctx, 0, sizeof(ipc_ctx));
    return;
}

