/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/* Standard include files */
#include <stdint.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

/* module specific include files */
#include <ipsecmgr_syslog.h>
#include "ipsecmgr_ipc_loc.h"

int ipsecmgr_ipc_unix_sock_init
(
    ipsecmgr_ipc_cfg_t  *cfg,
    ipc_unix_sock_ctx_t *ctx 
)
{
    struct sockaddr_un *my_addr = &(ctx->my_sock.sock_addr);
    struct sockaddr_un *cfg_addr = &(cfg->my_addr->sock_addr);

    if((ctx->my_sock_fd = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0)
    {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "IPC: ipc_unix_sock_init: error opening socket, errno=%d\n", errno);
        return -1;
    }

    *my_addr = *cfg_addr;

    unlink(my_addr->sun_path);

    if (bind(ctx->my_sock_fd, (const struct sockaddr *) my_addr, 
                            sizeof(struct sockaddr_un)) < 0)
    {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "IPC: ipc_unix_sock_init: error binding socket, errno=%d\n", errno);
        close(ctx->my_sock_fd);
        return -1;
    }
	
    ctx->nfds = ctx->my_sock_fd + 1;
    FD_ZERO(&ctx->readfds);
    FD_SET(ctx->my_sock_fd, &ctx->readfds);

    /* Set socket read timeout to zero */
    ctx->timeout.tv_sec = 0;
    ctx->timeout.tv_usec = 0;

    return 0;
}

int ipsecmgr_ipc_unix_sock_send
(
    ipc_unix_sock_ctx_t *ctx,
    uint8_t *buf,
    uint16_t len,
    ipsecmgr_ipc_addr_t *dest
)
{
    struct sockaddr_un *dst_addr = &(dest->sock_addr);

#if 0
    memset(&dst_addr, 0, sizeof(dst_addr));
    dst_addr.sun_family = AF_UNIX;
    strcpy(dst_addr.sun_path, *sock_name);
#endif

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO, 
        "IPC: ipc_unix_sock_send: sending to %s\n",
         dst_addr->sun_path);

    /* Send the message */
    if (sendto(ctx->my_sock_fd, buf, len, 0, (const struct sockaddr *)
               dst_addr, sizeof(struct sockaddr_un))==-1) 
    {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "IPC: ipc_unix_sock_send: error sending, errno=%d\n", errno);
        return -1;
    }
    
    return 0;
}

int ipsecmgr_ipc_unix_sock_recv
(
    ipc_unix_sock_ctx_t *ctx,
    uint8_t *buf,
    uint16_t *len,
    ipsecmgr_ipc_addr_t *src
)
{
    struct sockaddr_un *rem_addr = &(src->sock_addr);
    fd_set  read_fds = ctx->readfds;
    int retval, slen = sizeof(struct sockaddr_un);
    ssize_t rlen;
 
    retval = select(ctx->nfds, &read_fds, NULL, NULL, &ctx->timeout);
    if (retval < 0)
    {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "IPC: ipc_unix_sock_recv: error select, errno=%d\n", errno);
        return -1;
    } 

    if (retval == 0) {
        *len = 0;
        return 0;
    }

    if (FD_ISSET(ctx->my_sock_fd, &read_fds)) {
        if ((rlen = recvfrom(ctx->my_sock_fd, buf, *len, 0, 
                     (struct sockaddr *)rem_addr, &slen)) == -1) {
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR, "IPC: ipc_unix_sock_recv: error recvfrom, errno=%d\n", errno);
            return -1;
        }
    } else {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR, "IPC: ipc_unix_sock_recv: bad recv file descriptor\n");
        return -1;
    }

    *len = rlen;
    return 0;
}

void ipsecmgr_ipc_unix_socket_close(ipc_unix_sock_ctx_t *ctx)
{
    struct sockaddr_un *my_addr = &(ctx->my_sock.sock_addr);
    close(ctx->my_sock_fd);
    unlink(my_addr->sun_path);
}

