/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef __IPSECMGR__IPC_LOC_H__
#define __IPSECMGR__IPC_LOC_H__

#include <stdint.h>
#include <ipsecmgr_ipc.h>

typedef struct {
    ipsecmgr_ipc_addr_t rem_sock;
    ipsecmgr_ipc_addr_t my_sock;
    int                 my_sock_fd;
    struct timeval      timeout;
    int                 nfds;
    fd_set              readfds;
} ipc_unix_sock_ctx_t;

int ipc_unix_sock_init
(
    ipsecmgr_ipc_cfg_t  *cfg,
    ipc_unix_sock_ctx_t *ctx 
);

int ipc_unix_sock_send
(
    ipc_unix_sock_ctx_t *ctx,
    uint8_t             *buf,
    uint16_t            len,
    ipsecmgr_ipc_addr_t *dest
);

int ipc_unix_sock_recv
(
    ipc_unix_sock_ctx_t *ctx,
    uint8_t             *buf,
    uint16_t            *len,
    ipsecmgr_ipc_addr_t *src
);

#endif /* __IPSECMGR__IPC_LOC_H__ */

