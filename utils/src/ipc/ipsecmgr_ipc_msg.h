/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef __IPSECMGR__IPC_MSG_H__
#define __IPSECMGR__IPC_MSG_H__

#include <stdint.h>
#include <ipsecmgr_types.h>

#ifndef PACK_ATTR
/* Pack attribute for GCC */
#define PACK_ATTR __attribute__((packed))
#endif

/******************************************************************************
 MESSAGE to ipsecmgr daemon
******************************************************************************/
/* Message OFFLOAD_SP */
#define IPSECMGR_IPC_MSG_OFFLOAD_SP_VALID_IFNAME    0x1
#define IPSECMGR_IPC_MSG_OFFLOAD_SP_VALID_DSCP_CFG  0x2
#define IPSECMGR_IPC_MSG_OFFLOAD_SP_VALID_L5_SEL    0x4

struct OFFLOAD_SP {
    uint32_t                valid_param;

    /* Security Policy ID in the kernel */
    ipsecmgr_policy_id_t    policy_id;

    /* MAC interface name; required only for ingress policy */
    ipsecmgr_ifname_t       if_name;

    /* DSCP mapping for IPSec tunnel; required only for egress policy */
    ipsecmgr_sa_dscp_map_cfg_t dscp_cfg;

    ipsecmgr_l5_selector_t  l5_selector;

    ipsecmgr_sa_flags_t     sa_flags;
} PACK_ATTR;

/* Message OFFLOAD_SP_RSP */
struct OFFLOAD_SP_RSP {
    ipsecmgr_rsp_type_t     type;
    ipsecmgr_result_t       result;

    /* NETCP lib err code */
    uint32_t                err_code;

    /* FP handle for associated SA*/
    ipsecmgr_fp_handle_t    sa_handle;

     /* FP handle for SP */
    ipsecmgr_fp_handle_t    sp_handle;
} PACK_ATTR;

/* Message STOP_OFFLOAD */
struct STOP_OFFLOAD {
    /* Security Policy ID in the kernel */
    ipsecmgr_policy_id_t    policy_id;
    ipsecmgr_no_expire_sa_t no_expire_sa;
} PACK_ATTR;

/* Message STOP_OFFLOAD */
struct STOP_OFFLOAD_RSP {
    ipsecmgr_rsp_type_t     type;
    ipsecmgr_result_t       result;
} PACK_ATTR;

/* Message Identifiers */
#define MSG_ID_OFFLOAD_SP       1
#define MSG_ID_OFFLOAD_SP_RSP   2
#define MSG_ID_STOP_OFFLOAD     3
#define MSG_ID_STOP_OFFLOAD_RSP 4

union MSG_BODY {
	struct OFFLOAD_SP       offload_sp;
	struct OFFLOAD_SP_RSP   offload_sp_rsp;
	struct STOP_OFFLOAD     stop_offload;
	struct STOP_OFFLOAD_RSP stop_offload_rsp;
} PACK_ATTR;

typedef struct {
    /* Message identifier MSG_ID_* */
    uint32_t    	msg_id;

    /* To corelate request & response */
    ipsecmgr_trans_id_t trans_id;

    /* len of the message in bytes excluding this header */
    uint32_t    	msg_len;
} PACK_ATTR msg_hdr_t;

typedef struct {
    msg_hdr_t       hdr;
    union MSG_BODY  body;
} PACK_ATTR msg_t;

#endif /* __IPSECMGR__IPC_MSG_H__ */

