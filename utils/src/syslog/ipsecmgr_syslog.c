/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/* Standard include files */
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

/* module specific include files */
#include <ipsecmgr_syslog.h>

/* Environment variable name for log file */
#define LOG_FILE_ENV    "IPSECMGR_LOG_FILE"

static enum ipsecmgr_syslog_level_e curr_level = SYSLOG_LEVEL_INFO;
static FILE *log_file = NULL;

void ipsecmgr_syslog_msg(enum ipsecmgr_syslog_level_e level, char *msg, ...)
{
    va_list fmtargs;
    char buffer[1024];

    va_start(fmtargs,msg);
    vsnprintf(buffer,sizeof(buffer)-1,msg,fmtargs);
    va_end(fmtargs);

    if ((level >= curr_level) && (log_file)) {
        fprintf(log_file, "%s", buffer);
        fflush(log_file);
    }

    return;
}

void ipsecmgr_syslog_set_level(enum ipsecmgr_syslog_level_e level)
{
    curr_level = level;
}

#define MAX_LEN_LOG_FILE_NAME	128

static int32_t ipsecmgr_syslog_open_log_file(void)
{
    char *name;

    /* Check if already initialized */
    if (log_file) return 0;

    name = getenv(LOG_FILE_ENV);

    if ((name == NULL) || (strlen(name) >= MAX_LEN_LOG_FILE_NAME)){
        printf("ipsecmgr: syslog: Failed to get log file name\n");
        return -1;
    }

    log_file = fopen(name, "a");

    if (!log_file) {
        printf("ipsecmgr: syslog: Failed to open log file (%s)\n", name);
        return -1;
    }

    return 0;
}

int32_t ipsecmgr_syslog_init(void)
{
    return ipsecmgr_syslog_open_log_file();
}

void ipsecmgr_syslog_close(void)
{
    if (log_file) fclose(log_file);
    log_file = NULL;
    return;
}

