#!/bin/sh
# Source this script before starting the ipsecmgr daemon

# Variable to specify the unix socket name of the IPSec daemon
export IPSECMGR_DAEMON_SOCK_NAME="/etc/ipsd_sock"
# Variable to specify the log file to be used by the ipsecmgr library
export IPSECMGR_LOG_FILE="/var/run/ipsecmgr_daemon.log"

