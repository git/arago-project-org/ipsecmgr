#!/bin/sh
# Modify & source this script before starting the build

ARAGO_DIR=~/sc-mcsdk/arago-tmp/sysroots/armv7a-none-linux-gnueabi

# Cross tool prefix
export CROSS_COMPILE=arm-none-linux-gnueabi-
export CC=${CROSS_COMPILE}gcc

# libnl library path
export LIBNL_LIB_DIR=$ARAGO_DIR/usr/lib

