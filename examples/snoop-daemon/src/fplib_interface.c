/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/* Standard includes */
#include <stdio.h>
#include <arpa/inet.h>
#include <inttypes.h>

/* ipsecmgr includes */
#include <ipsecmgr_snoop.h>
#include <ipsecmgr_syslog.h>

#include "fplib_interface.h"

uint32_t fplib_sa_handle = 0x1000;
uint32_t fplib_sp_handle = 0x1000;

extern int debug_rekey;

int fplib_if_add_sa
(
    ipsecmgr_af_t           af,
    ipsecmgr_sa_id_t        *sa_id,
    ipsecmgr_sa_info_t      *sa_info,
    ipsecmgr_sa_dscp_map_cfg_t *dscp_map_cfg,
    ipsecmgr_ifname_t       *if_name,
    ipsecmgr_sa_encap_tmpl_t *encap,
    ipsecmgr_fp_handle_t    *sa_handle
)
{
    char addr_str[48];
    char *ip_str_addr;
    int i;

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "fplib_if_add_sa: spi(%d)\n", sa_id->spi);

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "fplib_if_add_sa: reqid(%d)\n", sa_info->reqid);

    if (sa_info->flags & IPSECMGR_SA_FLAGS_REKEY) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "fplib_if_add_sa: rekey SA\n");
    }

    if (af == IPSECMGR_AF_IPV4) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sa: af(IPV4)\n");
        ip_str_addr = (char *)inet_ntop(AF_INET, sa_id->daddr.ipv4,
                       addr_str, sizeof(addr_str));
        if (ip_str_addr) {
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sa: daddr(%s)\n", ip_str_addr);
        }
        ip_str_addr = (char *)inet_ntop(AF_INET, sa_info->saddr.ipv4,
                       addr_str, sizeof(addr_str));
        if (ip_str_addr) {
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sa: saddr(%s)\n", ip_str_addr);
        }
    } else if (af == IPSECMGR_AF_IPV6) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sa: af(IPV6)\n");
        ip_str_addr = (char *)inet_ntop(AF_INET6, sa_id->daddr.ipv6,
                       addr_str, sizeof(addr_str));
        if (ip_str_addr) {
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sa: daddr(%s)\n", ip_str_addr);
        }
        ip_str_addr = (char *)inet_ntop(AF_INET6, sa_info->saddr.ipv6,
                       addr_str, sizeof(addr_str));
        if (ip_str_addr) {
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sa: saddr(%s)\n", ip_str_addr);
        }
    } else {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "fplib_if_add_sa: invalid af(%d)\n", af);
        return -1;
    }

    if (sa_info->dir == DIR_OUTBOUND) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sa: direction(outbound)\n");
    } else if (sa_info->dir == DIR_INBOUND) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sa: direction(inbound)\n");
    } else {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "fplib_if_add_sa: invalid direction(%d)\n", sa_info->dir);
        return -1;
    }

    if (sa_id->proto == SA_PROTO_ESP) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sa: proto(ESP)\n");
    } else if (sa_id->proto == SA_PROTO_AH) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sa: proto(AH)\n");
    } else {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "fplib_if_add_sa: invalid proto(%d)\n", sa_id->proto);
        return -1;
    }

    if (sa_info->mode == SA_MODE_TRANSPORT) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sa: mode(transport)\n");
    } else if (sa_info->mode == SA_MODE_TUNNEL) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sa: mode(tunnel)\n");
    } else {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "fplib_if_add_sa: invalid mode(%d)\n", sa_info->mode);
        return -1;
    }

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "fplib_if_add_sa: replay_win(%d)\n", sa_info->replay_window);

    if (sa_info->flags & IPSECMGR_SA_FLAGS_ESN) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "fplib_if_add_sa: ESN enabled\n");
    }

    switch (sa_info->auth.algo) {
        case SA_AALG_HMAC_MD5:
        case SA_AALG_HMAC_SHA1:
        case SA_AALG_HMAC_SHA2_256:
        case SA_AALG_HMAC_SHA2_224:
        case SA_AALG_AES_XCBC:
        case SA_AALG_NULL:
        case SA_AALG_NONE:
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sa: aalg(%d)\n", sa_info->auth.algo);
            break;
        default:
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "fplib_if_add_sa: invalid aalg(%d)\n", sa_info->auth.algo);
            return -1;
    }

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "fplib_if_add_sa: icvlen(%d)\n", sa_info->auth.icvlen);

    switch (sa_info->enc.algo) {
        case SA_EALG_AES_CTR:
        case SA_EALG_3DES_CBC:
        case SA_EALG_DES_CBC:
        case SA_EALG_AES_CBC:
        case SA_EALG_AES_CCM:
        case SA_EALG_AES_GCM:
        case SA_EALG_NULL:
        case SA_EALG_NONE:
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sa: ealg(%d)\n", sa_info->enc.algo);
            break;
        default:
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "fplib_if_add_sa: invalid ealg(%d)\n", sa_info->enc.algo);
            return -1;
    }

    if (sa_info->enc_key_len > IPSECMGR_SA_INFO_MAX_KEY_LEN) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
        "fplib_if_add_sa: invalid enc_key_len(%d)\n", sa_info->enc_key_len);
    }

    if (sa_info->auth_key_len > IPSECMGR_SA_INFO_MAX_KEY_LEN) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
        "fplib_if_add_sa: invalid auth_key_len(%d)\n",
        sa_info->auth_key_len);
    }

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "fplib_if_add_sa: enc_key_len(%d)\n", sa_info->enc_key_len);

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "fplib_if_add_sa: auth_key_len(%d)\n", sa_info->auth_key_len);

    if (sa_info->enc_key_len) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
                            "fplib_if_add_sa: enc_key(0x");
        for (i=0; i<sa_info->enc_key_len; i++) {
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
                                "%2x", sa_info->enc_key[i]);
        }
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO, ")\n");
    }

    if (sa_info->auth_key_len) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
                            "fplib_if_add_sa: auth_key(0x");
        for (i=0; i<sa_info->auth_key_len; i++) {
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
                                "%2x", sa_info->auth_key[i]);
        }
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO, ")\n");
    }

    if (if_name) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "fplib_if_add_sa: if_name(%s)\n", if_name->name);
    }

    if (dscp_map_cfg) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "fplib_if_add_sa: dscp_map_cfg type(%d)\n", dscp_map_cfg->type);
    }

    /* lifetime config */
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "fplib_if_add_sa: lft_soft_byte_limit(%"PRIu64")\n", sa_info->lft.soft_byte_limit);
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "fplib_if_add_sa: lft_hard_byte_limit(%"PRIu64")\n", sa_info->lft.hard_byte_limit);
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "fplib_if_add_sa: lft_soft_pkt_limit(%"PRIu64")\n", sa_info->lft.soft_packet_limit);
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "fplib_if_add_sa: lft_hard_pkt_limit(%"PRIu64")\n", sa_info->lft.hard_packet_limit);
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "fplib_if_add_sa: lft_soft_add_expires(%"PRIu64")\n", sa_info->lft.soft_add_expires);
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "fplib_if_add_sa: lft_hard_add_expires(%"PRIu64")\n", sa_info->lft.hard_add_expires);
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "fplib_if_add_sa: lft_soft_use_expires(%"PRIu64")\n", sa_info->lft.soft_use_expires);
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "fplib_if_add_sa: lft_hard_use_expires(%"PRIu64")\n", sa_info->lft.hard_use_expires);

    if (encap) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sa: encap:sport(%d)\n", encap->sport);
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sa: encap:dport(%d)\n", encap->dport);
    }

    *sa_handle = fplib_sa_handle;
    fplib_sa_handle++;
    return 0;
}

int fplib_if_add_sp
(
    ipsecmgr_af_t           af,
    ipsecmgr_selector_t     *sel,
    ipsecmgr_dir_t          dir,
    uint32_t                reqid,
    ipsecmgr_fp_handle_t    sa_handle,
    ipsecmgr_policy_id_t    policy_id,
    ipsecmgr_fp_handle_t    *sp_handle
)
{
    char addr_str[48];
    char *ip_str_addr;

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sp: policy_id(%d)\n", policy_id);

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sp: reqid(%d)\n", reqid);

    if (af == IPSECMGR_AF_IPV4) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sp: af(IPV4)\n");
        ip_str_addr = (char *)inet_ntop(AF_INET, sel->daddr.ipv4,
                       addr_str, sizeof(addr_str));
        if (ip_str_addr) {
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sp: daddr(%s)\n", ip_str_addr);
        }
        ip_str_addr = (char *)inet_ntop(AF_INET, sel->saddr.ipv4,
                       addr_str, sizeof(addr_str));
        if (ip_str_addr) {
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sp: saddr(%s)\n", ip_str_addr);
        }
    } else if (af == IPSECMGR_AF_IPV6) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sp: af(IPV6)\n");
        ip_str_addr = (char *)inet_ntop(AF_INET6, sel->daddr.ipv6,
                       addr_str, sizeof(addr_str));
        if (ip_str_addr) {
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sp: daddr(%s)\n", ip_str_addr);
        }
        ip_str_addr = (char *)inet_ntop(AF_INET6, sel->saddr.ipv6,
                       addr_str, sizeof(addr_str));
        if (ip_str_addr) {
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sp: saddr(%s)\n", ip_str_addr);
        }
    } else {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "fplib_if_add_sp: invalid af(%d)\n", af);
        return -1;
    }

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sp: prefixlen_s(%d)\n", sel->prefixlen_s);

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sp: prefixlen_d(%d)\n", sel->prefixlen_d);

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sp: proto(%d)\n", sel->proto);

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sp: dport_start(%d)\n", sel->dport_start);

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sp: dport_end(%d)\n", sel->dport_end);

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sp: sport_start(%d)\n", sel->sport_start);

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sp: sport_end(%d)\n", sel->sport_end);

    if (dir == DIR_OUTBOUND) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sp: direction(outbound)\n");
    } else if (dir == DIR_INBOUND) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sp: direction(inbound)\n");
    } else {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "fplib_if_add_sp: invalid direction(%d)\n", dir);
        return -1;
    }

    if (sel->l5_selector) {

        if (sel->l5_selector->proto != IPSECMGR_L5_PROTO_GTPU) {
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
                "fplib_if_add_sp: invalid l5 proto(%d)\n",
                sel->l5_selector->proto);
            return -1;
        } else {
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
                "fplib_if_add_sp: l5 proto(%d)\n",
                sel->l5_selector->proto);
	}

        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
                "fplib_if_add_sp: teid_start(%d)\n",
                sel->l5_selector->value.gtpu.teid_start);
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
                "fplib_if_add_sp: teid_end(%d)\n",
                sel->l5_selector->value.gtpu.teid_end);
    }

    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_add_sp: sa_handle(0x%x)\n", sa_handle);

    *sp_handle = fplib_sp_handle;
    fplib_sp_handle++;
    debug_rekey++;
    return 0;
}

int fplib_if_del_sp
(
    ipsecmgr_fp_handle_t   sp_handle,
    ipsecmgr_policy_id_t   policy_id,
    ipsecmgr_dir_t         dir
)
{
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_del_sp: sp_handle(0x%x)\n", sp_handle);
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_del_sp: policy_id(%d)\n", policy_id);
    return 0;
}

int fplib_if_del_sa
(
    ipsecmgr_fp_handle_t   sa_handle 
)
{
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_del_sa: sa_handle(0x%x)\n", sa_handle);
    return 0;
}

int fplib_if_get_sa_ctx
(
    ipsecmgr_fp_handle_t    sa_handle,
    ipsecmgr_sa_hw_ctx_t    *hw_ctx
)
{
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
            "fplib_if_get_s_ctx: sa_handle(0x%x)\n", sa_handle);
    hw_ctx->swinfo_sz = 2;
    hw_ctx->swinfo[0] = 0x10004003;
    hw_ctx->swinfo[1] = 0xa0294200;
    hw_ctx->flow_id = 1;
    return 0;
}

