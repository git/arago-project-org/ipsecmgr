/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/* Standard includes */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <time.h>
#include <pthread.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

/* ipsecmgr includes */
#include <ipsecmgr_snoop.h>
#include <ipsecmgr_ipc.h>
#include <ipsecmgr_syslog.h>

#include "fplib_interface.h"

/* Lock file for the daemon */
#define LOCK_FILE   "/var/lock/ipsecmgr_daemon"

/* termination signal handler */
static void termination_handler(int signum);

typedef pthread_t task_handle;

#define DEFAULT_STACK_SIZE	0x8000
static int task_create ( void *(start_routine)(void*), void* args, void* handle)
{
    int max_priority, err;
    pthread_t thread;
    pthread_attr_t attr;
    struct sched_param param;

    max_priority = sched_get_priority_max(SCHED_FIFO);
    err = pthread_attr_init(&attr);
    if (err) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "pthread_attr_init failed: (%s)\n", strerror(err));
        return err;
    }
    err = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    if (err) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "pthread_attr_setdetachstate failed: (%s)\n", strerror(err));
        return err;
    }
    err = pthread_attr_setstacksize(&attr, DEFAULT_STACK_SIZE);
    if (err) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "pthread_attr_setstacksize failed: (%s)\n", strerror(err));
        return err;
    }
    err = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    if (err) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "pthread_attr_setinheritsched failed: (%s)\n", strerror(err));
        return err;
    }
    err = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
    if (err) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "pthread_attr_setschedpolicy failed: (%s)\n", strerror(err));
        return err;
    }
    memset(&param, 0, sizeof(param));
    param.sched_priority = max_priority;
    err = pthread_attr_setschedparam(&attr, &param);
    if (err) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "pthread_attr_setschedparam failed: (%s)\n", strerror(err));
        return err;
    }
    if (err) return err;
    err = pthread_create(&thread, &attr, start_routine, args);
    if (err) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "pthread_create failed: (%s)\n", strerror(err));
        return err;
    }
    if (err) return err;
    *(pthread_t*)handle = thread;
    return 0;
}

static void task_wait (void *handle)
{
    pthread_join(*((pthread_t*)handle), NULL);
    return;
}

static void task_stop (task_handle handle)
{
    pthread_cancel(((pthread_t)handle));
    return;
}

static void task_sleep(uint32_t time_in_msec)
{
    pthread_mutex_t fake_mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_cond_t fake_cond = PTHREAD_COND_INITIALIZER;
    struct timespec ts;
    int rt;
    unsigned int sec, nsec;

    sec = time_in_msec/1000;
    nsec = (time_in_msec - (sec*1000)) * 1000000;

    /* Use the wall-clock time */
    clock_gettime(CLOCK_REALTIME, &ts);

    ts.tv_sec += sec;
    ts.tv_nsec += nsec;

    pthread_mutex_lock(&fake_mutex);
    rt = pthread_cond_timedwait(&fake_cond, &fake_mutex, &ts);
    pthread_mutex_unlock(&fake_mutex);
}

/* snoop task */
task_handle snoop_run_th;


//#define TEST_REKEY

int debug_rekey = 0;
extern uint32_t fplib_sa_handle;

#ifdef TEST_REKEY
static void expire_sa(int hard, ipsecmgr_fp_handle_t sa_handle)
{
    ipsecmgr_lft_cur_t lft; 

    if (hard) {
        lft.bytes = 9000;
        lft.packets = 300;
    }
    else
    {
        lft.bytes = 7000;
        lft.packets = 200;
    }
    lft.add_time = 0;
    lft.use_time = 0;

    if (ipsecmgr_snoop_sa_expiry(sa_handle, hard, &lft))
    {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
        "snoop_run_task: expiry failed\n");
    }
}
#endif

static void *snoop_run_task(void *args)
{
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "snoop_run_task: daemon entering forever event loop\n");

    while (1) {
        /* Poll for message from user application */
        ipsecmgr_ipc_poll();

        /* Poll for message from Kernel */
        ipsecmgr_snoop_run();

#ifdef TEST_REKEY
        if(debug_rekey == 1)
        {
            ipsecmgr_snoop_run();
            ipsecmgr_snoop_run();
            expire_sa(0, fplib_sa_handle-1);
            debug_rekey++;
        }
#endif
    }

    return;
}

static ipsecmgr_ipc_daemon_send_if_t *send_iface;

static int offload_sp_rsp_send
(
    ipsecmgr_snoop_offload_sp_rsp_param_t *rsp,
    void                                  *addr,
    uint32_t                              addr_size
)
{
    ipsecmgr_ipc_offload_sp_rsp_param_t offload_sp_rsp;

    offload_sp_rsp.type = rsp->type;
    offload_sp_rsp.result = rsp->result;
    offload_sp_rsp.trans_id = rsp->trans_id;
    offload_sp_rsp.err_code = rsp->err_code;
    offload_sp_rsp.sp_handle = rsp->sp_handle;
    offload_sp_rsp.sa_handle = rsp->sa_handle;

    if (addr_size != sizeof(ipsecmgr_ipc_addr_t)) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "offload_sp_rsp_send: addr size not correct\n");
        return -1;
    }

    return send_iface->offload_sp_rsp(&offload_sp_rsp,
                                      (ipsecmgr_ipc_addr_t *)addr);
}

static int stop_offload_rsp_send
(
    ipsecmgr_snoop_stop_offload_rsp_param_t *rsp,
    void                                    *addr,
    uint32_t                                addr_size
)
{
    ipsecmgr_ipc_stop_offload_rsp_param_t stop_offload_rsp;

    stop_offload_rsp.type = rsp->type;
    stop_offload_rsp.result = rsp->result;
    stop_offload_rsp.trans_id = rsp->trans_id;

    if (addr_size != sizeof(ipsecmgr_ipc_addr_t)) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "stop_offload_rsp_send: addr size not correct\n");
        return -1;
    }

    return send_iface->stop_offload_rsp(&stop_offload_rsp,
                                      (ipsecmgr_ipc_addr_t *)addr);
}

static int rekey_event_handler
(
    ipsecmgr_snoop_rekey_event_param_t  *params,
    void                                *addr,
    uint32_t                            addr_size
)
{
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "rekey_event: sp_id(%d), result(%d), sa_handle(0x%x)\n",
        params->policy_id, params->result, params->rekey_sa_handle);
#ifdef TEST_REKEY
    expire_sa(1, fplib_sa_handle-2);
    debug_rekey = 1;
#endif
    return 0;
}

static void offload_sp_req_recv
(
    ipsecmgr_ipc_offload_sp_req_param_t *req,
    ipsecmgr_ipc_addr_t                 *src_addr
)
{
    ipsecmgr_snoop_offload_sp_rsp_param_t rsp;
    uint32_t addr_size = sizeof(ipsecmgr_ipc_addr_t);
    ipsecmgr_snoop_offload_sp_req_param_t offload_sp_req;

    offload_sp_req.trans_id = req->trans_id;
    offload_sp_req.policy_id = req->policy_id;
    offload_sp_req.sa_flags = req->sa_flags;
    offload_sp_req.if_name = req->if_name;
    offload_sp_req.dscp_cfg = req->dscp_cfg;
    offload_sp_req.l5_selector = req->l5_selector;

    memset(&rsp, 0, sizeof(rsp));
    rsp.trans_id = req->trans_id;

    if (ipsecmgr_snoop_offload_sp_req(&offload_sp_req, (void*)src_addr,
                                      addr_size)) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "offload_sp_req_recv: snoop_offload_sp_req failed\n");
        rsp.result = RESULT_FAILURE;
        rsp.type = RSP_TYPE_ACK | RSP_TYPE_DONE;
    }
    else
    {
        rsp.result = RESULT_SUCCESS;
        rsp.type = RSP_TYPE_ACK;
    }
    
    if (offload_sp_rsp_send(&rsp, (void *)src_addr, addr_size))
    {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "offload_sp_req_recv: failed to send ACK for offload_sp\n");
    }

    return;
}

static void stop_offload_req_recv
(
    ipsecmgr_ipc_stop_offload_req_param_t   *req,
    ipsecmgr_ipc_addr_t                     *src_addr
)
{
    ipsecmgr_snoop_stop_offload_req_param_t stop_offload_req;
    uint32_t addr_size = sizeof(ipsecmgr_ipc_addr_t);
    ipsecmgr_snoop_stop_offload_rsp_param_t rsp;

    stop_offload_req.trans_id = req->trans_id;
    stop_offload_req.policy_id = req->policy_id;
    stop_offload_req.no_expire_sa = req->no_expire_sa;

    memset(&rsp, 0, sizeof(rsp));
    rsp.trans_id = req->trans_id;

    if (ipsecmgr_snoop_stop_offload(&stop_offload_req, (void*)src_addr,
                                      addr_size)) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "stop_offload_req_recv: snoop_stop_offload failed\n");
        rsp.result = RESULT_FAILURE;
        rsp.type = RSP_TYPE_ACK | RSP_TYPE_DONE;
    }
    else
    {
        rsp.result = RESULT_SUCCESS;
        rsp.type = RSP_TYPE_ACK;
    }

    if (stop_offload_rsp_send(&rsp, (void *)src_addr, addr_size))
    {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "stop_offload_req_recv: failed to send ACK for stop_offload\n");
    }

    return;
}

int snoop_run()
{
    struct ipsecmgr_snoop_fp_cfg_cb     fp_cfg_cb;
    struct ipsecmgr_snoop_mgnt_cb       mgnt_cb;
    struct ipsecmgr_snoop_platform_cb   plat_cb;
    ipsecmgr_ipc_daemon_recv_if_t       recv_if;
    ipsecmgr_ipc_cfg_t                  ipc_cfg;
    int status;

    /* Initializations */
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "snoop_run: starting initialization\n");

    memset(&fp_cfg_cb, 0, sizeof(fp_cfg_cb));
    memset(&mgnt_cb, 0, sizeof(mgnt_cb));
    memset(&plat_cb, 0, sizeof(plat_cb));
    memset(&recv_if, 0, sizeof(recv_if));
    memset(&ipc_cfg, 0, sizeof(ipc_cfg));

    /* Initialize IPC library */
    ipc_cfg.mode = IPC_MODE_SNOOP_DAEMON;
    if (ipsecmgr_ipc_init(&ipc_cfg)) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "snoop_run: ipc_init failed\n");
        return -1;
    }

    if (ipsecmgr_ipc_get_daemon_send_iface(&send_iface)) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "snoop_run: ipc_get_daemon_send_iface failed\n");
        return -1;
    }

    recv_if.offload_sp_req = offload_sp_req_recv;
    recv_if.stop_offload_req = stop_offload_req_recv;

    /* Register ipsecmgr daemon recieve i/f */
    if (ipsecmgr_ipc_register_daemon_recv_iface(&recv_if)) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "snoop_run: ipc_register_daemon_recv_iface failed\n");
        return -1;
    }

    /* Initialize the snoop library */
    fp_cfg_cb.add_sa = fplib_if_add_sa;
    fp_cfg_cb.add_sp = fplib_if_add_sp;
    fp_cfg_cb.del_sa = fplib_if_del_sa;
    fp_cfg_cb.del_sp = fplib_if_del_sp;
    fp_cfg_cb.get_sa_ctx = fplib_if_get_sa_ctx;

    plat_cb.log_msg  = (ipsecmgr_snoop_log_msg_t)ipsecmgr_syslog_msg;
    plat_cb.sleep    = task_sleep;

    mgnt_cb.offload_sp_rsp = offload_sp_rsp_send;
    mgnt_cb.stop_offload_rsp = stop_offload_rsp_send;
    mgnt_cb.rekey_event = rekey_event_handler;

    status = ipsecmgr_snoop_init(&fp_cfg_cb, &mgnt_cb, &plat_cb);
    if (status) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "snoop_run: ipsecmgr_snoop_init failed (%d)\n", status);
        return (-1);
    }

    /* Create the task context for snoop library */
    if (status = task_create(snoop_run_task, NULL, &snoop_run_th)) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "snoop_run: snoop run task-create failed (%d)\n", status);
        return (-1);
    }

    /* Setup signal handler for SIGTERM */
    if (signal(SIGTERM, termination_handler) == SIG_ERR) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_WARN,
            "snoop_run: cannot handle SIGTERM\n");
    }

    task_wait(&snoop_run_th);
    return 0;
}

/* termination signal handler */
static void termination_handler(int signum)
{
    ipsecmgr_snoop_shutdown();
    exit(0);
}

static void daemonize( const char *lock_file )
{
    pid_t pid, sid;
    int lock_fp = -1;
    char str[10];

    /* already a daemon */
    if ( getppid() == 1 ) return;

    /* Fork off the parent process */
    pid = fork();
    if (pid < 0) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "daemonize: unable to fork daemon, code=%d (%s)",
            errno, strerror(errno) );
        exit(-1);
    }

    /* If we got a PID, then exit the parent process. */
    if (pid > 0) {
        exit(0);
    }

    /* At this point we are executing as the child process */

    /* Create a new SID for the child process */
    sid = setsid();
    if (sid < 0) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "daemonize: unable to create a new session, code %d (%s)",
            errno, strerror(errno) );
        exit(-1);
    }

    /* Change the file mode mask */
    umask(027);

    /* Change the current working directory.  This prevents the current
       directory from being locked; hence not being able to remove it. */
    if ((chdir("/")) < 0) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "daemonize: unable to change directory to %s, code %d (%s)",
            "/", errno, strerror(errno));
        exit(-1);
    }

    /* Create the lock file */
    if ( lock_file ) {
        lock_fp = open(lock_file, O_RDWR|O_CREAT, 0640);
        if ( lock_fp < 0 ) {
            ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
                "daemonize: unable to create lock file %s, code=%d (%s)",
                lock_file, errno, strerror(errno));
            exit(-1);
        }
    }

    if (lockf(lock_fp,F_TLOCK,0)<0) exit(-1); /* can not lock */

    sprintf(str,"%d\n",getpid());
    write(lock_fp,str,strlen(str)); /* record pid to lockfile */

    /* Cancel certain signals */
    signal(SIGCHLD,SIG_DFL); /* A child process dies */
    signal(SIGTSTP,SIG_IGN); /* Various TTY signals */
    signal(SIGTTOU,SIG_IGN);
    signal(SIGTTIN,SIG_IGN);
    signal(SIGHUP, SIG_IGN); /* Ignore hangup signal */
    signal(SIGTERM,termination_handler); /* catch SIGTERM */

    /* Redirect standard files to /dev/null */
    freopen( "/dev/null", "r", stdin);
    freopen( "/dev/null", "w", stdout);
    freopen( "/dev/null", "w", stderr);

    return;
}

/* usage: ipsecmgr_daemon
 */
int main (int argc, char *argv[])
{
    /* Initialize syslog */
    ipsecmgr_syslog_init();

    daemonize(LOCK_FILE);

    snoop_run();
    return 0;
}

