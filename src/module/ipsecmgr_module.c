/*
 * File name: ipsecmgr_module.c
 *
 * Description: IPSecMgr Kernel module.
 *
 * Copyright (C) 2012 Texas Instruments, Incorporated
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/sort.h>
#include <linux/bsearch.h>
#include <linux/xfrm.h>
#include <linux/version.h>

#include <net/net_namespace.h>

#include <linux/list.h>
#include <linux/hash.h>

#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/netfilter_ipv6.h>

#include "ipsecmgr_mod_loc.h"

#define MAX_NUM_OFFLOADED_SA 32
#define MAX_REFERENCE_COUNT 10

#define HASH_BITS 3
#define HASH_TBL_SZ (1 << HASH_BITS)

static DEFINE_SPINLOCK(esp_in_lock);
static DEFINE_SPINLOCK(esp_out_lock);
static int esp_in_num_sa = 0;
static int esp_out_num_sa = 0;

struct hlist_head esp_in_hash_tbl[HASH_TBL_SZ] =
{[0 ... (HASH_TBL_SZ-1)] = HLIST_HEAD_INIT};

struct hlist_head esp_out_hash_tbl[HASH_TBL_SZ] =
{[0 ... (HASH_TBL_SZ-1)] = HLIST_HEAD_INIT};

static struct class *ipsecmgr_mod_class;
static int ipsecmgr_mod_major;
static struct device *ipsecmgr_dev;
static atomic_t reference_count = ATOMIC_INIT(0);

#define AF_NOT_INET(af) ((af) != AF_INET && (af) != AF_INET6)

/* Hash table utilities */
static inline void hash_tbl_init(struct hlist_head *tbl)
{
	int i;
	for (i = 0; i < HASH_TBL_SZ; i++)
		INIT_HLIST_HEAD(&tbl[i]);
}

static inline void hash_tbl_free(struct hlist_head *tbl)
{
	int i;
	struct ipsecmgr_mod_sa_info *entry;

	for (i = 0; i < HASH_TBL_SZ; i++) {
		if (hlist_empty(&tbl[i]))
			continue;

		hlist_for_each_entry(entry, &tbl[i], byspi) {
			hlist_del(&entry->byspi);
			kfree(entry);
		}
	}
}

static inline uint32_t esp4_spi_hash (uint32_t spi, xfrm_address_t *daddr)
{
	return hash_32((spi ^ daddr->a4), HASH_BITS);
}

static inline uint32_t esp6_spi_hash(uint32_t spi, xfrm_address_t *daddr)
{
	return hash_32((spi ^ (daddr->a6[2] ^ daddr->a6[3])),HASH_BITS);
}

static struct ipsecmgr_mod_sa_info
*esp_lookup_byspi(uint32_t spi, xfrm_address_t *daddr, struct hlist_head *tbl,
		  bool is_esp4)
{
	struct ipsecmgr_mod_sa_info *entry;
	uint32_t bkt = (is_esp4) ? esp4_spi_hash(spi, daddr):
		esp6_spi_hash(spi, daddr);

	hlist_for_each_entry(entry, &tbl[bkt], byspi) {
		if (entry->spi != spi)
			continue;
		if (is_esp4) {
			if (entry->daddr.a4 != daddr->a4)
				continue;
		} else {
			if(memcmp(entry->daddr.a6, daddr->a6,
				  sizeof(daddr->a6)))
				continue;
		}
		return entry;
	}

	return NULL;
}

static inline void esp_insert_byspi(struct ipsecmgr_mod_sa_info *sa_info,
				    struct hlist_head *tbl, bool is_esp4)
{
	uint32_t bkt = (is_esp4) ? esp4_spi_hash(sa_info->spi, &sa_info->daddr):
		esp6_spi_hash(sa_info->spi, &sa_info->daddr);

	hlist_add_head(&sa_info->byspi, &tbl[bkt]);
	return;
}

static inline int esp_del_byspi(struct ipsecmgr_mod_sa_info *sa_info)
{
	hlist_del(&sa_info->byspi);
	return 0;
}

int add_sa_ctx(struct ipsecmgr_mod_sa_info *sa_info,
	       struct sk_buff *skb)
{
	ipsecmgr_mod_sa_ctx_info_internal_t  *sa_ctx;

	sa_ctx = (ipsecmgr_mod_sa_ctx_info_internal_t *) secpath_dup(NULL);
	if (!sa_ctx)
		return -1;

	sa_ctx->majic = IPSECMAN_MAJIC;
	sa_ctx->swinfo_0 = sa_info->sa_ctx.swinfo_0;
	sa_ctx->swinfo_1 = sa_info->sa_ctx.swinfo_1;
	sa_ctx->flow_id = sa_info->sa_ctx.flow_id;
	skb->sp =(struct sec_path *)sa_ctx;
	return 0;
}

static long ipsecmgr_mod_ioctl(struct file *filp, unsigned int cmd,
			       unsigned long args)
{
	struct ipsecmgr_mod_user_sa_params sa_params;
	spinlock_t  *lock;
	unsigned int __user *argp = (unsigned int __user *) args;

	if (_IOC_TYPE(cmd) != _IOC_TYPE(IPSECMGR_MOD_IOCMAGIC)) {
		__E("ioctl(): bad command type 0x%x (should be 0x%x)\n",
		    _IOC_TYPE(cmd), _IOC_TYPE(IPSECMGR_MOD_IOCMAGIC));
	}

	cmd &= IPSECMGR_MOD_IOCCMDMASK;
	__D("cmd %d ioctl received.\n", cmd);

	if (copy_from_user(&sa_params, argp, sizeof(sa_params)))
		return -EFAULT;

	__D("dir(%d) spi(0x%x) proto(%d) af(%d) sw0(0x%x) sw1(0x%x) flow_id(%d)\n",
	    sa_params.dir, sa_params.spi, sa_params.proto, sa_params.af,
	    sa_params.sa_ctx.swinfo_0, sa_params.sa_ctx.swinfo_1,
	    sa_params.sa_ctx.flow_id);

	if (AF_NOT_INET(sa_params.af)) {
		__E("OFFLOAD_SA: unsupported address family(%d).\n", sa_params.af);
		return -EINVAL;
	}

	if (sa_params.proto != IPPROTO_ESP) {
		__E("OFFLOAD_SA: unsupported IP protocol(%d).\n", sa_params.proto);
		return -EINVAL;
	}

	switch (cmd) {
	case IPSECMGR_MOD_IOC_OFFLOAD_SA:
	{
		struct hlist_head *esp_hash_tbl;
		struct ipsecmgr_mod_sa_info *sa_info;
		struct ipsecmgr_mod_sa_info *sa_info_temp;
		int *num_sa;

		__D("OFFLOAD_SA: daddr(0x%x)\n", sa_params.daddr.a4);

		if (sa_params.dir == IPSECMGR_MOD_DIR_IN) {
			esp_hash_tbl = esp_in_hash_tbl;
			num_sa = &esp_in_num_sa;
			lock = &esp_in_lock;
		} else if (sa_params.dir == IPSECMGR_MOD_DIR_OUT) {
			esp_hash_tbl = esp_out_hash_tbl;
			num_sa = &esp_out_num_sa;
			lock = &esp_out_lock;
		} else {
			__E("OFFLOAD_SA: Unknown direction.\n");
			return -EINVAL;
		}
		sa_info = kzalloc(sizeof(*sa_info), GFP_KERNEL);

		if(!sa_info)
			return -ENOMEM;

		if (*num_sa == MAX_NUM_OFFLOADED_SA)
			return -ENOSPC;

		sa_info->spi = htonl(sa_params.spi);
		sa_info->daddr = sa_params.daddr;
		sa_info->sa_ctx = sa_params.sa_ctx;
		INIT_HLIST_NODE(&sa_info->byspi);

		spin_lock_bh(lock);

		sa_info_temp = esp_lookup_byspi(sa_info->spi,
						&sa_info->daddr,
						esp_hash_tbl,
						sa_params.af == AF_INET);

		if (sa_info_temp) {
			sa_info_temp->ref_count++;
			spin_unlock_bh(lock);
			kfree(sa_info);
			return 0;
		}

		esp_insert_byspi(sa_info, esp_hash_tbl, sa_params.af == AF_INET);
		sa_info->ref_count++;
		spin_unlock_bh(lock);
		*num_sa += 1;
		break;
	}

	case IPSECMGR_MOD_IOC_CHECK_SA_EXPIRE:
	{
		xfrm_address_t *xaddr;
		struct xfrm_state *xstate;
		struct net *net = &init_net;

		xaddr = (xfrm_address_t *)&sa_params.daddr;
		__D("CHECK_SA_EXPIRE: daddr(0x%x)\n", xaddr->a4);

		xstate = xfrm_state_lookup(net, 0, xaddr,
					   htonl(sa_params.spi), sa_params.proto,
					   sa_params.af);
		if (xstate == NULL) {
			__E("CHECK_SA_EXPIRE: could find xfrm state\n");
			return -EINVAL;
		}
		xfrm_state_put(xstate);

		if (xfrm_state_check_expire(xstate)) {
			__D("CHECK_SA_EXPIRE: xfrm_sate_check_expire failed\n");
			return -EINVAL;
		}
		break;
	}

	case IPSECMGR_MOD_IOC_STOP_OFFLOAD:
	{
		struct hlist_head *esp_hash_tbl;
		struct ipsecmgr_mod_sa_info *sa_info;
		uint32_t spi;
		xfrm_address_t daddr;
		int *num_sa;

		__D("STOP_OFFLOAD: daddr(0x%x)\n", sa_params.daddr.a4);

		if (sa_params.dir == IPSECMGR_MOD_DIR_IN) {
			esp_hash_tbl = esp_in_hash_tbl;
			num_sa = &esp_in_num_sa;
			lock = &esp_in_lock;
		} else if (sa_params.dir == IPSECMGR_MOD_DIR_OUT) {
			esp_hash_tbl = esp_out_hash_tbl;
			num_sa = &esp_out_num_sa;
			lock = &esp_out_lock;
		} else {
			__E("STOP_OFFLOAD: Unknown direction.\n");
			return -EINVAL;
		}

		if (!*num_sa) {
			__E("STOP_OFFLOAD: No offloaded SA.\n");
			return -EINVAL;
		}

		spi = htonl(sa_params.spi);
		daddr = sa_params.daddr;

		spin_lock_bh(lock);

		sa_info = esp_lookup_byspi(spi, &daddr, esp_hash_tbl,
					   (sa_params.af == AF_INET));
		if (!sa_info) {
			__E("STOP_OFFLOAD: SA not offloaded.\n");
			spin_unlock_bh(lock);
			return -EINVAL;
		}
		if(sa_info->ref_count) {
			sa_info->ref_count--;
			if(sa_info->ref_count) {
				spin_unlock_bh(lock);
				return 0;
			}
			esp_del_byspi(sa_info);
			kfree(sa_info);
		} else {
			__E("STOP_OFFLOAD: SA not offloaded.\n");
			spin_unlock_bh(lock);
			return -EINVAL;
		}

		spin_unlock_bh(lock);
		*num_sa -= 1;
		break;
	}

	default:
		__E("Unknown ioctl received.\n");
		return -EINVAL;
	}
	return 0;
}

static int ipsecmgr_mod_open(struct inode *inode, struct file *filp)
{
	__D("open: called.\n");

	if (atomic_read(&reference_count) > MAX_REFERENCE_COUNT) {
		__E("open: device already in use.\n");
		return -EBUSY;
	}

	atomic_inc(&reference_count);
	return 0;
}

static int ipsecmgr_mod_release(struct inode *inode, struct file *filp)
{
	__D("close: called.\n");

	atomic_dec(&reference_count);
	return 0;
}

struct ipsecmgr_mod_sa_info*
ipsecmgr_mod_esp_is_offloaded_sa(struct xfrm_state *x, int dir, bool is_esp4)
{
	spinlock_t *lock;
	struct hlist_head *tbl;
	struct ipsecmgr_mod_sa_info *sa_info;

	if (dir == IPSECMGR_MOD_DIR_IN) {
		tbl = (void*)esp_in_hash_tbl;
		lock = &esp_in_lock;
	} else {
		tbl = (void*)esp_out_hash_tbl;
		lock = &esp_out_lock;
	}

	spin_lock_bh(lock);
	sa_info = esp_lookup_byspi(x->id.spi, &x->id.daddr, tbl, is_esp4);

	spin_unlock_bh(lock);

	return sa_info;
}

struct ipsecmgr_mod_sa_info*
ipsecmgr_mod_esp4_is_offloaded_sa(struct xfrm_state *x, int dir)
{
	return ipsecmgr_mod_esp_is_offloaded_sa(x, dir, true);
}

struct ipsecmgr_mod_sa_info*
ipsecmgr_mod_esp6_is_offloaded_sa(struct xfrm_state *x, int dir)
{
	return ipsecmgr_mod_esp_is_offloaded_sa(x, dir, false);
}

/*
 * Netfilter LOCAL_OUT hook to force inner IP fragmentation
 * on UDP IPSec packets
 */
static unsigned int gso_hookv6(void *priv,
			       struct sk_buff *skb,
			       const struct nf_hook_state *state)
{
	struct dst_entry *dst;
	struct xfrm_state *x;
	const struct ipv6hdr *iphv6;

	if(!skb)
		return NF_ACCEPT;

	dst = skb_dst(skb);
	x = dst->xfrm;

	if (!x)
		return NF_ACCEPT;

	if (skb->len <= dst_mtu(dst))
		return NF_ACCEPT;

	iphv6 = ipv6_hdr(skb);

	if ((iphv6->version != IPVERSION) && (iphv6->version != 6))
		return NF_ACCEPT;
	if (iphv6->nexthdr != IPPROTO_UDP)
		return NF_ACCEPT;

	if (!ipsecmgr_mod_esp6_is_offloaded_sa(x, IPSECMGR_MOD_DIR_OUT) &&
	    !ipsecmgr_mod_esp4_is_offloaded_sa(x, IPSECMGR_MOD_DIR_OUT))
		return NF_ACCEPT;

	skb->ip_summed = CHECKSUM_PARTIAL;
	skb->csum = 0;
	skb_shinfo(skb)->gso_size = ((dst_mtu(dst) - 80) & ~7);
	skb_shinfo(skb)->gso_type = SKB_GSO_UDP;
	return NF_ACCEPT;
}

static unsigned int gso_hook(void *priv,
			     struct sk_buff *skb,
			     const struct nf_hook_state *state)
{
	struct dst_entry *dst;
	struct xfrm_state *x;
	const struct iphdr *iph;

	if(!skb)
		return NF_ACCEPT;

	dst = skb_dst(skb);
	x = dst->xfrm;

	if (!x)
		return NF_ACCEPT;

	if (skb->len <= dst_mtu(dst))
		return NF_ACCEPT;

	iph = ip_hdr(skb);

	if (iph->version != IPVERSION)
		return NF_ACCEPT;

	if ((iph->protocol != IPPROTO_UDP) && (iph->protocol != IPPROTO_ICMP))
		return NF_ACCEPT;

	if (!ipsecmgr_mod_esp6_is_offloaded_sa(x, IPSECMGR_MOD_DIR_OUT) &&
	    !ipsecmgr_mod_esp4_is_offloaded_sa(x, IPSECMGR_MOD_DIR_OUT))
		return NF_ACCEPT;

	skb->ip_summed = CHECKSUM_PARTIAL;
	skb->csum = 0;
	skb_shinfo(skb)->gso_size = ((dst_mtu(dst) - (iph->ihl * 4)) & ~7);
	skb_shinfo(skb)->gso_type = SKB_GSO_UDP;
	return NF_ACCEPT;
}

static struct nf_hook_ops netfilter_ops = {
	.hook		= gso_hook,
	.pf		= NFPROTO_IPV4,
	.hooknum	= NF_INET_LOCAL_OUT,
	.priority	= NF_IP_PRI_LAST,
};

static struct nf_hook_ops netfilter_opsv6 = {
	.hook		= gso_hookv6,
	.pf		= NFPROTO_IPV6,
	.hooknum	= NF_INET_LOCAL_OUT,
	.priority	= NF_IP6_PRI_LAST,
};

static struct file_operations file_ops = {
	.owner		= THIS_MODULE,
	.unlocked_ioctl	= ipsecmgr_mod_ioctl,
	.open		= ipsecmgr_mod_open,
	.release	= ipsecmgr_mod_release,
};

static int __init ipsecmgr_mod_init(void)
{
	int ret;

	if ((ret=esp4_init())) {
		__E("Failed to initialize esp4 module.\n");
		return ret;
	}

	if ((ret=esp6_init())) {
		__E("Failed to initialize esp6 module.\n");
		return ret;
	}

	ipsecmgr_mod_major = register_chrdev(0, IPSECMGR_MOD_DEVNAME,
					     &file_ops);

	if (ipsecmgr_mod_major < 0) {
		__E("Failed to allocate major number.\n");
		ret = -ENODEV;
		goto cleanup_proto;
	}

	ipsecmgr_mod_class = class_create(THIS_MODULE, IPSECMGR_MOD_DEVNAME);
	if (IS_ERR(ipsecmgr_mod_class)) {
		__E("Error creating device class.\n");
		ret = PTR_ERR(ipsecmgr_mod_class);
		goto cleanup_dev;
	}

	ipsecmgr_dev = device_create(ipsecmgr_mod_class, NULL,
				     MKDEV(ipsecmgr_mod_major, 0), NULL,
				     IPSECMGR_MOD_DEVNAME);

	if (IS_ERR(ipsecmgr_dev)) {
		__E("Error creating device.\n");
		ret = PTR_ERR(ipsecmgr_dev);
		goto cleanup_class;
	}

	hash_tbl_init(esp_in_hash_tbl);
	hash_tbl_init(esp_out_hash_tbl);

	if (nf_register_hook(&netfilter_ops)) {
		__E("Error adding NF hook.\n");
		ret = -EFAULT;
		goto cleanup_class;
	}

	if (nf_register_hook(&netfilter_opsv6)) {
		__E("Error adding NF hookv6.\n");
		ret = -EFAULT;
		goto cleanup_class;
	}

	return 0;

cleanup_class:
	class_destroy(ipsecmgr_mod_class);

cleanup_dev:
	unregister_chrdev(ipsecmgr_mod_major, IPSECMGR_MOD_DEVNAME);

cleanup_proto:
	esp4_fini();
	esp6_fini();

	return ret;
}

static void __exit ipsecmgr_mod_exit(void)
{
	class_destroy(ipsecmgr_mod_class);
	unregister_chrdev(ipsecmgr_mod_major, IPSECMGR_MOD_DEVNAME);
	esp4_fini();
	esp6_fini();
	hash_tbl_free(esp_in_hash_tbl);
	hash_tbl_free(esp_out_hash_tbl);
	nf_unregister_hook(&netfilter_ops);
	nf_unregister_hook(&netfilter_opsv6);
}

module_init(ipsecmgr_mod_init);
module_exit(ipsecmgr_mod_exit);
MODULE_LICENSE("GPL");
MODULE_ALIAS_XFRM_TYPE(AF_INET, XFRM_PROTO_ESP);

