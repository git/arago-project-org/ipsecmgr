/*
 * File name: ipsecmgr_mod_loc.h
 *
 * Description: IPSecMgr Kernel module local defines.
 *
 * Copyright (C) 2012 Texas Instruments, Incorporated
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef __TI_IPSECMGR_MOD_LOC_H__
#define __TI_IPSECMGR_MOD_LOC_H__

#include <net/xfrm.h>

#include "ipsecmgr_mod.h"

//#define IPSECMGR_MOD_DEBUG

#ifdef IPSECMGR_MOD_DEBUG
#  define __D(fmt, args...) printk(KERN_DEBUG "IPSECMGR Debug: " fmt, ## args)
#else
#  define __D(fmt, args...)
#endif

#define __E(fmt, args...) printk(KERN_ERR "IPSECMGR Error: " fmt, ## args)

#define IPSECMAN_MAJIC 0xdeadfeed

struct ipsecmgr_mod_sa_info {
	struct hlist_node           byspi;
	u32                         spi;
	xfrm_address_t              daddr;
	u32                         ref_count;
	ipsecmgr_mod_sa_ctx_info_t  sa_ctx;
};
typedef struct {
	/* 1st 2 elements should of this strucutre should not be moved, intention is to
	   mimic struct sec_path */
	atomic_t  refcnt;
	int       len;       /* set to 0 */
	int       majic;     /* set to  IPSECMAN_MAJIC */
	uint32_t  swinfo_0;  /* SWINFO word 0 */
	uint32_t  swinfo_1;  /* SWINFO word 0 */
	uint16_t  flow_id;   /* SA Rx DMA flow-id */
} ipsecmgr_mod_sa_ctx_info_internal_t;

struct ipsecmgr_mod_sa_info*
ipsecmgr_mod_esp4_is_offloaded_sa(struct xfrm_state *x, int dir);

struct ipsecmgr_mod_sa_info*
ipsecmgr_mod_esp6_is_offloaded_sa(struct xfrm_state *x, int dir);


int add_sa_ctx(struct ipsecmgr_mod_sa_info *sa_info, struct sk_buff *skb);

static inline void *esp_pskb_put(struct sk_buff *skb, struct sk_buff *tail, int len)
{
	if (tail != skb) {
		skb->data_len += len;
		skb->len += len;
	}
	return skb_put(tail, len);
}

int esp4_init(void);
void esp4_fini(void);
int esp6_init(void);
void esp6_fini(void);
#endif

